﻿namespace MvkVPNConnectUtility
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.ServiceProcess;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using DotRas;
    using MvkVPNConnectUtility.Properties;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// The Virtual Private Network server's host name.
        /// </summary>
        public const string HostName = "vpn.kremen.org.ua";

        /// <summary>
        /// The connection (entry in user Phonebook) name.
        /// </summary>
        public const string ConnectionName = "UGIS vpn";

        /// <summary>
        /// Gets or sets the dialer device (PPTP).
        /// </summary>
        public static RasDevice DialerDevice { get; set; }

        /// <summary>
        /// Gets or sets the phone book which contains all user Virtual Private Network connection profiles.
        /// </summary>
        public static RasPhoneBook PhoneBook { get; set; }

        /// <summary>
        /// Gets or sets the worker entry(new Virtual Private Name connection entry).
        /// </summary>
        public static RasEntry Worker { get; set; }

        /// <summary>
        /// Gets or sets the dialer which invoke call to VPN process.
        /// </summary>
        public static RasDialer Dialer { get; set; }

        /// <summary>
        /// Gets or sets the Vpn connection handle.
        /// </summary>
        public static RasHandle ConnectionHandle { get; set; }

        /// <summary>
        /// Gets or sets the login string.
        /// </summary>
        public static string LoginString { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            ConnectionHandle = null;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(Settings.Default.Language); 
            try
            {
                DialerDevice = RasDevice.GetDeviceByName("PPTP", RasDeviceType.Vpn, false);
            }
            catch (Exception e)
            {
                try
                {
                    this.TryRasmanStart(e);
                    DialerDevice = RasDevice.GetDeviceByName("PPTP", RasDeviceType.Vpn, false);
                }
                catch (Exception rasmanException)
                {
                    MessageBox.Show(rasmanException.Message);
                    Environment.Exit(-1);
                }
            }
            
            PhoneBook = new RasPhoneBook();
            PhoneBook.Open(true);
            Dialer = new RasDialer { PhoneBookPath = PhoneBook.Path, EntryName = ConnectionName };           
            this.WindowModel = new MainViewModel();
            this.InitializeComponent();
            this.DataContext = this.WindowModel;
            this.WindowModel.LoginButtonText = Properties.Resources.ConnectButtonTextConnect;            
            LoginString = Properties.Resources.ConnectButtonTextConnect;
            Dialer.DialCompleted += this.DialerDialCompleted;
            Dialer.StateChanged += this.DialerStateChanged;
            Dialer.Error += this.Dialer_Error;
            Dialer.Disposed += this.Dialer_Disposed;
            PhoneBook.Error += this.PhoneBook_Error;            
        }

        /// <summary>
        /// Gets or sets the main window view model. See more at MVVM pattern for MS WPF
        /// </summary>
        public MainViewModel WindowModel { get; set; }

        /// <summary>
        /// Shutdown all active connections to Virtual Private Network server's defined by <see cref="HostName"/> and named by <see cref="ConnectionName"/>.
        /// </summary>
        public static void ShutdownConnections()
        {
            var connections = from cons in RasConnection.GetActiveConnections()
                              where cons.EntryName == ConnectionName
                              select cons;
            if (connections.Any())
            {
                foreach (var connection in connections)
                {
                    connection.HangUp();
                }
            }
        }

        /// <summary>
        /// The on window (and application) closing.
        /// </summary>
        /// <param name="e">
        /// The e. Cancel event arguments.
        /// </param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {            
            if (MessageBox.Show(
                Properties.Resources.AppCloseDialogText,
                Properties.Resources.AppCloseDialogTitle,
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                e.Cancel = false;
                ShutdownConnections();
                base.OnClosing(e);
                return;
            }

            e.Cancel = true;
        }

        /// <summary>
        /// The phone book_ error handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e. Event arguments.
        /// </param>
        private void PhoneBook_Error(object sender, System.IO.ErrorEventArgs e)
        {
            MessageBox.Show(e.GetException().Message);
        }

        /// <summary>
        /// The dialer_ disposed event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Dialer_Disposed(object sender, EventArgs e)
        {
            ConnectionHandle = null;
        }

        /// <summary>
        /// The dialer_ error event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Dialer_Error(object sender, System.IO.ErrorEventArgs e)
        {
            MessageBox.Show(e.GetException().Message);
        }

        /// <summary>
        /// The dialer state changed event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private async void DialerStateChanged(object sender, StateChangedEventArgs e)
        {
            Debug.WriteLine(e.State.ToString());
            if (e.State != RasConnectionState.Connected)
            {
                this.WindowModel.LoginButtonText = Properties.Resources.ConnectButtonTextConnect;

                return;
            }

            ////-------------------------------
            /// Start new asynchronous thread  for connection monitoring. Used anonymous method         
            var status = await Task<bool>.Factory.StartNew(
                () =>
                    {
                        for (;;)
                        {
                            var connections = from cons in RasConnection.GetActiveConnections() where cons.EntryName == ConnectionName select cons;
                        if (!connections.Any())
                        {
                            return false;
                        }

                        Thread.Sleep(5000);
                    }
                });

            if (!status)
            {
                Dialer.DialAsyncCancel();
                this.WindowModel.LoginButtonText = Properties.Resources.ConnectButtonTextConnect;
            }

            if (e.State == RasConnectionState.Disconnected)
            {
                MessageBox.Show("dis");
            }
        }

        /// <summary>
        /// The dialer dial completed event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void DialerDialCompleted(object sender, DialCompletedEventArgs e)
        {
            this.WindowModel.LoginButtonText = Properties.Resources.ConnectButtonTextReConnect;
        }

        /// <summary>
        /// The try Remote Access Service start.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <exception cref="Exception">
        /// Raises when cant start Remote Access Service
        /// </exception>
        private void TryRasmanStart(Exception exception)
        {
            var rasmanServiceController = new ServiceController("RasMan");
            switch (rasmanServiceController.Status)
            {
                case ServiceControllerStatus.Running:
                    {
                        MessageBox.Show(exception.Message);
                        MessageBox.Show("Try to restart PC or contact your admin");
                        Environment.Exit(-1);
                        break;
                    }

                case ServiceControllerStatus.Stopped:
                    {
                        try
                        {
                            rasmanServiceController.Start();
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.Message);
                            Environment.Exit(-1);
                        }

                        break;
                    }

                case ServiceControllerStatus.Paused:
                    {
                        try
                        {
                            rasmanServiceController.Continue();
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.Message);
                            Environment.Exit(-1);
                        }

                        break;
                    }

                default:
                    {
                        throw exception;
                    }
            }
        }
    }
}
