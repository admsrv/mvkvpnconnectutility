﻿namespace MvkVPNConnectUtility
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public class BaseViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// The property changed event raise when user try to input new token of math expression.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The notify property changed event body.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}