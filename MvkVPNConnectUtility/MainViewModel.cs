﻿namespace MvkVPNConnectUtility
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Input;

    using DotRas;

    using Microsoft.TeamFoundation.MVVM;

    using MvkVPNConnectUtility.Properties;

    public class MainViewModel : BaseViewModel
    {
        private delegate RasEntry CreateEntryDelegate();

        public MainViewModel()
        {
            this.LoginClick = new RelayCommand(this.OnLoginClick, this.CanLoginClickExec);
            this.CreateClick = new RelayCommand(this.OnCreateClick, this.CanCreateClickExec);
            this.ShowHideClick = new RelayCommand(this.OnShowHide);
            this.ExitClick = new RelayCommand(this.OnExit);
            this.SaveLangClick = new RelayCommand(this.OnSaveLang);
            this.AppLangs = new ObservableCollection<CultureInfo>()
                                {
                                    CultureInfo.CreateSpecificCulture("en-US"),
                                    CultureInfo.CreateSpecificCulture("uk-UA")
                                };
            this.currentLang = Thread.CurrentThread.CurrentUICulture;
        }

        private CultureInfo currentLang;

        public CultureInfo CurrentAppLang
        {
            get
            {
                return this.currentLang;
            }
            set
            {
                this.currentLang = value;
                Settings.Default.Language = this.currentLang.IetfLanguageTag;
                Settings.Default.Save();
                MessageBox.Show(Properties.Resources.MessageLangChanged);
                this.NotifyPropertyChanged("CurrentAppLang");
            }
        }

        private void OnSaveLang(object param)
        {
            var langString = param as string;
            if (langString != null)
            {
                
            }
        }

        private void OnShowHide()
        {
            if (Application.Current.MainWindow.IsVisible == true)
            {
                Application.Current.MainWindow.Visibility = Visibility.Hidden;
            }
            else
            {
                Application.Current.MainWindow.Visibility = Visibility.Visible;
            }
        }
        public ICommand SaveLangClick { get; set; }
        public ObservableCollection<CultureInfo> AppLangs { get; set; }
        public ICommand LoginClick { get; set; }
        public ICommand CreateClick { get; set; }

        public ICommand ShowHideClick { get; set; }

        public ICommand ExitClick { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        private string loginButtonText;

        private string createButtonText;
        public string LoginButtonText
        {
            get
            {
                return this.loginButtonText;
            }
            set
            {
                this.loginButtonText = value;
                this.NotifyPropertyChanged("LoginButtonText");
            }
        }

        public string CreateButtonText
        {
            get
            {
                return this.createButtonText;
            }
            set
            {
                this.createButtonText = value;
                this.NotifyPropertyChanged("CreateButtonText");
            }
        }
        public bool CanCreateClickExec(object param)
        {
            if (MainWindow.Dialer == null || MainWindow.DialerDevice == null
                || MainWindow.PhoneBook == null || MainWindow.ConnectionHandle != null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(this.Login) || string.IsNullOrWhiteSpace(this.Login))
            {
                return false;
            }

            if (string.IsNullOrEmpty(this.Password) || string.IsNullOrWhiteSpace(this.Password))
            {
                return false;
            }

            return true;
        }

        public void OnExit()
        {
            Application.Current.MainWindow.Close();
        }

        public void OnLoginClick(object param)
        {           
            try
            {
                MainWindow.ShutdownConnections();
                MainWindow.Dialer.AllowUseStoredCredentials = true;
                 MainWindow.ConnectionHandle = MainWindow.Dialer.DialAsync();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public void OnCreateClick(object param)
        {
          MainWindow.PhoneBook.Open(true);
          CreateEntryDelegate maker =
                () =>
                RasEntry.CreateVpnEntry(
                    MainWindow.ConnectionName,
                    MainWindow.HostName,
                    RasVpnStrategy.PptpOnly,
                    MainWindow.DialerDevice);

            var rasValidator = new RasEntryNameValidator
            {
                EntryName = MainWindow.ConnectionName,
                PhoneBookPath = MainWindow.PhoneBook.Path
            };
            rasValidator.Validate();
            if (rasValidator.IsValid)
            {
                MainWindow.Worker = maker();
                try
                {
                    MainWindow.PhoneBook.Entries.Add(MainWindow.Worker);
                }
                catch(Exception e)
                {
                    if (MainWindow.PhoneBook.Entries.Contains(MainWindow.ConnectionName))
                    {
                        MessageBox.Show(e.Message);
                        return;
                    }                    
                }

                MainWindow.Dialer.Credentials = new System.Net.NetworkCredential(this.Login, this.Password);
            }
            else
            {
                if (MainWindow.PhoneBook.Entries.Contains(MainWindow.ConnectionName))
                {
                    try
                    {
                        MainWindow.PhoneBook.Entries.Remove(MainWindow.ConnectionName);
                        MainWindow.Worker = maker();
                        MainWindow.PhoneBook.Entries.Add(MainWindow.Worker);
                        foreach (var conn in MainWindow.PhoneBook.Entries.Where(conn => conn.Name == MainWindow.ConnectionName))
                        {
                            conn.Options.RemoteDefaultGateway = false;
                            conn.Update();
                            conn.UpdateCredentials(new System.Net.NetworkCredential(this.Login, this.Password));                            
                        }

                        MainWindow.Dialer.Credentials = new System.Net.NetworkCredential(this.Login, this.Password);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                        MessageBox.Show("Please, restart PC");                        
                    }
                }
            }            
        }

        public bool CanLoginClickExec(object param)
        {
            if (MainWindow.Dialer.IsBusy)
            {
                return false;
            }            

            var rasValidator = new RasEntryNameValidator
            {
                EntryName = MainWindow.ConnectionName,
                PhoneBookPath = MainWindow.PhoneBook.Path,
                AllowExistingEntries = true
            };
            rasValidator.Validate();
            return rasValidator.IsValid;
        }        
    }
}
